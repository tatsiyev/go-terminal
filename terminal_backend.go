package goterminal

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type TerminalBackend struct {
}

func TerminalHttp(password string, backend *TerminalBackendInfo) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		bodyBytes, e := ioutil.ReadAll(r.Body)
		if e != nil {
			w.Write([]byte(e.Error()))
			return
		}
		body := string(bodyBytes)
		rows := strings.Split(body, "\n")
		if len(rows) != 2 {
			w.Write([]byte("invalid data format"))
			return
		}
		pass, inp := rows[0], rows[1]

		if pass != password {
			w.Write([]byte("invalid password"))
			return
		}

		out := Action(backend, inp)
		w.Write([]byte(out))
	}
}
func RunTerminalBackend(conf *TerminalConf, port int, password string) error {

	backend := newTerminalBackendInfo(conf)

	mux := http.NewServeMux()
	mux.HandleFunc("/terminal", TerminalHttp(password, backend))
	return http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", port), mux)
}
