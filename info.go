package goterminal

import (
	"fmt"
	"reflect"
)

func Info(obj interface{}) string {
	v := reflect.ValueOf(obj)

	return info(v, 0)
}

func info(v reflect.Value, deep int) string {
	if deep == 3 {
		return ""
	}

	res := ""
	t := v.Type()
	k := t.Kind()

	deepPrefix := ""
	for range make([]int, deep+1) {
		deepPrefix += "    "
	}

	res += fmt.Sprintf("%sKind: '%s' Type: '%s' Name: '%s'\n", deepPrefix, k.String(), t, t.Name())

	switch k {
	case reflect.Pointer:

		e := v.Elem()
		ke := e.Kind()

		var i string

		i = info(e, deep+1)
		res += i

		if ke == reflect.Struct {
			for i := 0; i < v.NumMethod(); i++ {
				m := v.Method(i)
				mm := t.Method(i)

				symbol := "\u251C"
				if i == t.NumMethod()-1 {
					symbol = "\u2514"
				}

				res += fmt.Sprintf("%s%s %s %s\n", deepPrefix, symbol, mm.Name, m.Type())
			}
		}

	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			f := v.Field(i)
			tt := t.Field(i)

			symbol := "\u251C"
			if i == t.NumField()-1 {
				symbol = "\u2514"
			}

			res += fmt.Sprintf("%s%s %s %s\n", deepPrefix, symbol, tt.Name, f.Type())

			var inf string
			if f.Kind() == reflect.Ptr && !f.IsNil() {
				inf = info(f, deep+1)
			}

			res += inf
		}

		for i := 0; i < v.NumMethod(); i++ {
			m := v.Method(i)
			mm := t.Method(i)

			symbol := "\u251C"
			if i == t.NumField()-1 {
				symbol = "\u2514"
			}

			res += fmt.Sprintf("%s%s %s %s\n", deepPrefix, symbol, mm.Name, m.Type())

		}

	case reflect.Array:
	case reflect.Chan:
	case reflect.Func:
	case reflect.Interface:
	case reflect.Map:
	case reflect.Slice:
	case reflect.String:
	case reflect.UnsafePointer:

	}

	return res
}

func find(v reflect.Value, path []string) (reflect.Value, error) {
	if len(path) == 0 {
		return v, nil
	}

	current := path[0]

	t := v.Type()

	switch t.Kind() {
	case reflect.Invalid:
	case reflect.Bool:
	case reflect.Int:
	case reflect.Int8:
	case reflect.Int16:
	case reflect.Int32:
	case reflect.Int64:
	case reflect.Uint:
	case reflect.Uint8:
	case reflect.Uint16:
	case reflect.Uint32:
	case reflect.Uint64:
	case reflect.Uintptr:
	case reflect.Float32:
	case reflect.Float64:
	case reflect.Complex64:
	case reflect.Complex128:
	case reflect.Array:
	case reflect.Chan:
	case reflect.Func:
	case reflect.Interface:
	case reflect.Map:
		cur := reflect.ValueOf(current)
		exist := false
		for _, k := range v.MapKeys() {
			if cur.String() == k.String() {
				exist = true
				break
			}
		}
		if !exist {
			return reflect.ValueOf(nil), fmt.Errorf("field '%s' not found", current)
		}

		field := v.MapIndex(cur)
		if field.IsNil() {
			return reflect.ValueOf(nil), fmt.Errorf("field '%s' not found", current)
		}
		return find(field.Elem(), path[1:])

	case reflect.Pointer:
		e := v.Elem()
		find(e, path)
	case reflect.Slice:
	case reflect.String:
	case reflect.Struct:
		field := v.FieldByName(current)
		if field.IsNil() {
			return reflect.ValueOf(nil), fmt.Errorf("field '%s' not found", current)
		}
		return find(field, path[1:])

	case reflect.UnsafePointer:

	}

	return reflect.ValueOf(nil), fmt.Errorf("no match type %s on %s", t.Kind(), current)
}
