# go-terminal

# Installing
## Execute
```go install gitlab.com/tatsiyev/go-terminal/cmd/go-terminal@latest```
## Use
```go-terminal -h localhost -pass abc```
### command args
	-h (host) - example.com localhost
	-p (port) - 6600 7000
	-pass (password) - string row
	- c (command) - "scope.Info(scope.Data)"


# Code
## Local

```go
package main

import (
	term "gitlab.com/tatsiyev/go-terminal"
)

type Data struct {
	Val1   int
	Val2   string
	val3   float32
	inner  *Data
	inner2 *Data
}

func (d *Data) Action(v int) string {
	return "ok"
}

func main() {

	conf := &term.TerminalConf{
		Data: map[string]interface{}{
			"Ptr":  &Data{Val1: 4, Val2: "abc", val3: 4.5, inner: &Data{Val1: 7}, inner2: &Data{Val1: 7}},
			"Data": Data{Val1: 4, Val2: "abc", val3: 4.5, inner: &Data{Val1: 7}, inner2: &Data{Val1: 7}},
			"Simple":  3,
			"Mapping": map[string]int{"one": 1, "ten": 10},
			"Array":   []int{3, 14, 15, 9},
			"Fn":      func(x int, y string) error { return nil },
		}}

	close := make(chan error, 0)

    go func() { close <- term.RunTerminalLocal(conf) }()

	select {
	case <-close:
	}
}

```

## Only Backend

```go
package main

import (
	term "gitlab.com/tatsiyev/go-terminal"
)

type Data struct {
	Val1   int
	Val2   string
	val3   float32
	inner  *Data
	inner2 *Data
}

func (d *Data) Action(v int) string {
	return "ok"
}

func main() {

	conf := &term.TerminalConf{
		Data: map[string]interface{}{
			"Ptr":  &Data{Val1: 4, Val2: "abc", val3: 4.5, inner: &Data{Val1: 7}, inner2: &Data{Val1: 7}},
			"Data": Data{Val1: 4, Val2: "abc", val3: 4.5, inner: &Data{Val1: 7}, inner2: &Data{Val1: 7}},
			"Simple":  3,
			"Mapping": map[string]int{"one": 1, "ten": 10},
			"Array":   []int{3, 14, 15, 9},
			"Fn":      func(x int, y string) error { return nil },
		}}

	close := make(chan error, 0)

    go func() { close <- term.RunTerminalBackend(conf, 4500, "abc") }()
    
	select {
	case <-close:
	}
}

```

# Syntax
- You can use the golang syntax.
- Variables storage in `scope`.
- View the data structure with scope.Info

```
go_terminal(1)> scope.Simple * 3
9

go_terminal(2)> scope.Info(scope.Data)
    Kind: 'struct' Type: 'main.Data' Name: 'Data'
    ├ Val1 int
    ├ Val2 string
    ├ val3 float32
    ├ inner *main.Data
        Kind: 'ptr' Type: '*main.Data' Name: ''
            Kind: 'struct' Type: 'main.Data' Name: 'Data'
            ├ Val1 int
            ├ Val2 string
            ├ val3 float32
            ├ inner *main.Data
            └ inner2 *main.Data
        └ Action func(int) string
    └ inner2 *main.Data
        Kind: 'ptr' Type: '*main.Data' Name: ''
            Kind: 'struct' Type: 'main.Data' Name: 'Data'
            ├ Val1 int
            ├ Val2 string
            ├ val3 float32
            ├ inner *main.Data
            └ inner2 *main.Data
        └ Action func(int) string
```