package goterminal

import (
	"fmt"
	"reflect"
	"sync"
	"unsafe"

	"github.com/traefik/yaegi/interp"
	"github.com/traefik/yaegi/stdlib"
	"golang.org/x/term"
)

type TerminalConf struct {
	Unrestricted bool
	GoPath       string
	Args         []string
	BuildTags    []string
	Env          []string
	Data         map[string]interface{}
	Symbols      interp.Exports
}

type TerminalClientInfo struct {
	line       int
	screen     *term.Terminal
	termState  *term.State
	makeOutput func(string) string
}

type TerminalBackendInfo struct {
	interp *interp.Interpreter
	sync.Mutex
}

func makeVar(name string, value interface{}) string {
	addr := unsafe.Pointer(&value)
	return fmt.Sprintf("var %s = %v", name, addr)
}

func newTerminalBackendInfo(context *TerminalConf) *TerminalBackendInfo {
	i := interp.New(interp.Options{
		Unrestricted: context.Unrestricted,
		GoPath:       context.GoPath,
		Args:         context.Args,
		BuildTags:    context.BuildTags,
		Env:          context.Env,
	})

	i.Globals()

	i.Use(stdlib.Symbols)
	i.Use(context.Symbols)

	symbols := map[string]reflect.Value{}

	keys := make([]string, 0, len(context.Data))
	for k := range context.Data {
		keys = append(keys, k)
	}
	symbols["Scope"] = reflect.ValueOf(keys)

	for key, val := range context.Data {
		symbols[key] = reflect.ValueOf(val)
	}

	symbols["Info"] = reflect.ValueOf(Info)

	i.Use(map[string]map[string]reflect.Value{"scope/scope": symbols})
	i.Eval("import \"scope\"")

	return &TerminalBackendInfo{
		interp: i,
	}
}
