package goterminal

func MakeOutputLocal(back *TerminalBackendInfo) func(string) string {
	return func(cmd string) string {
		return Action(back, cmd)
	}
}

func RunTerminalLocal(conf *TerminalConf) error {
	backend := newTerminalBackendInfo(conf)

	term, e := NewClient()
	if e != nil {
		return e
	}

	term.makeOutput = MakeOutputLocal(backend)

	return clientMainFunc(term)
}
