package goterminal

import (
	"fmt"
	"os"

	"golang.org/x/term"
)

func getFd() int {
	return int(os.Stdin.Fd())
}

func NewClient() (*TerminalClientInfo, error) {
	termState, err := term.MakeRaw(getFd())
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to set raw mode on STDIN: %v\n", err)
		return nil, err
	}
	s := term.NewTerminal(os.Stdin, "> ")

	return &TerminalClientInfo{
		screen:    s,
		termState: termState,
	}, nil
}

func clientMainFunc(t *TerminalClientInfo) error {

	for {
		t.screen.SetPrompt(fmt.Sprintf("go_terminal(%d)> ", t.line+1))

		ln, err := t.screen.ReadLine()

		if err != nil {
			break
		}

		t.line++
		out := t.makeOutput(ln)

		t.screen.Write([]byte(out + "\n\n"))
	}

	term.Restore(getFd(), t.termState)
	fmt.Println()
	return nil
}
