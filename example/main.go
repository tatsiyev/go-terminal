package main

import (
	"flag"
	"fmt"
	"os"

	term "gitlab.com/tatsiyev/goterminal"
)

type Data struct {
	Val1   int
	Val2   string
	val3   float32
	inner  *Data
	inner2 *Data
}

func (d *Data) Action(v int) string {
	return "ok"
}

func main() {

	variant := flag.String("t", "local", "type terminal")
	flag.Parse()

	pwd, _ := os.Getwd()

	conf := &term.TerminalConf{
		GoPath: pwd,
		Data: map[string]interface{}{
			"Ptr":  &Data{Val1: 4, Val2: "abc", val3: 4.5, inner: &Data{Val1: 7}, inner2: &Data{Val1: 7}},
			"Data": Data{Val1: 4, Val2: "abc", val3: 4.5, inner: &Data{Val1: 7}, inner2: &Data{Val1: 7}},
			//"chan":    make(chan error),
			"Simple":  3,
			"Mapping": map[string]int{"one": 1, "ten": 10},
			"Array":   []int{3, 14, 15, 9},
			"Fn":      func(x int, y string) error { return nil },
		}}

	close := make(chan error, 0)

	var process func()

	fmt.Println(*variant)

	switch *variant {
	case "local":
		process = func() { close <- term.RunTerminalLocal(conf) }
	case "backend":
		process = func() { close <- term.RunTerminalBackend(conf, 4500, "abc") }
	case "client":
		process = func() { close <- term.RunTerminalClient("http://localhost:4500/terminal", "abc") }
	}

	go process()
	select {
	case <-close:
	}
}
