package main

import (
	"flag"
	"fmt"

	term "gitlab.com/tatsiyev/goterminal"
)

func main() {

	prot := flag.String("prot", "http", "protocol")
	host := flag.String("h", "localhost", "host")
	port := flag.String("p", "6600", "port")
	pass := flag.String("pass", "", "pass")
	command := flag.String("c", "", "command")
	flag.Parse()

	close := make(chan error, 0)

	url := fmt.Sprintf("%s://%s:%s/terminal", *prot, *host, *port)

	out := term.MakeOutputClient(url, *pass)(*command)

	if *command != "" {
		fmt.Println(out)
		return
	}

	if out != "<invalid reflect.Value>" { // error
		fmt.Println(out)
		return
	}

	go func() { close <- term.RunTerminalClient(url, *pass) }()

	select {
	case <-close:
	}
}
