package goterminal

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func MakeOutputClient(url, password string) func(string) string {
	return func(cmd string) string {

		method := "POST"
		payload := strings.NewReader(fmt.Sprintf("%s\n%s", password, cmd))

		client := &http.Client{}
		req, err := http.NewRequest(method, url, payload)

		if err != nil {
			return err.Error()
		}

		req.Header.Add("Content-Type", "text/plain")

		res, err := client.Do(req)
		if err != nil {
			return err.Error()
		}
		defer res.Body.Close()

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Println(err)
			return err.Error()
		}

		return string(body)
	}
}

func RunTerminalClient(url, password string) error {
	term, e := NewClient()
	if e != nil {
		return e
	}

	term.makeOutput = MakeOutputClient(url, password)

	return clientMainFunc(term)
}
