module gitlab.com/tatsiyev/goterminal

go 1.20

require (
	github.com/traefik/yaegi v0.15.1
	golang.org/x/term v0.7.0
)

require golang.org/x/sys v0.7.0 // indirect
