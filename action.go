package goterminal

import "fmt"

func Action(backendInfo *TerminalBackendInfo, inp string) string {
	out, e := backendInfo.interp.Eval(inp)
	if e != nil {
		return e.Error()
	}
	return fmt.Sprintf("%v", out)
}
